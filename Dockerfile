FROM fedora

RUN groupadd -g 1010 mireg
RUN useradd -u 1010 -g 1010 mireg

RUN mkdir /data

RUN chown mireg:mireg /data
USER mireg

EXPOSE 5000
WORKDIR /data

