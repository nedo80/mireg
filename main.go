package main

import (
	"crypto/sha256"
	"encoding/json"
	"fmt"
	"html/template"
	"io"
	"io/fs"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"path"
	"regexp"
	"sort"
	"strings"

	"github.com/google/uuid"
	"github.com/gorilla/handlers"
	"github.com/gorilla/pat"
)

type Schema struct {
	Version int `json:"schemaVersion"`
	Config  struct {
		MediaType string `json:"mediaType"`
		Digest    string `json:"digest"`
		Size      int64  `json:"size"`
	} `json:"config"`
	Layers []struct {
		MediaType string `json:"mediaType"`
		Digest    string `json:"digest"`
		Size      int64  `json:"size"`
	} `json:"layers"`
}

func validateName(name string) bool {
	matched, err := regexp.Match(`^[a-zA-Z0-9/]*$`, []byte(name))
	if err == nil && matched {
		return true
	}
	return false
}

func validateTag(tag string) bool {
	matched, err := regexp.Match(`^[a-zA-Z0-9\.]*$`, []byte(tag))
	if err == nil && matched {
		return true
	}
	return false
}

func validateDigest(digest string) bool {
	matched, err := regexp.Match(`^[a-zA-Z0-9:]*$`, []byte(digest))
	if err == nil && matched {
		return true
	}
	return false
}

func PrepRepo(repo string) error {
	err := os.MkdirAll(repo+"/tags", 0744)
	if err != nil {
		return err
	}
	err = os.MkdirAll(repo+"/blobs", 0744)
	if err != nil {
		return err
	}
	err = os.MkdirAll(repo+"/uploads", 0744)
	if err != nil {
		return err
	}
	return nil
}

func Accepts(r *http.Request, t string) bool {
	for _, hi := range r.Header.Values("Accept") {
		for _, val := range strings.Split(hi, ",") {
			if strings.TrimSpace(val) == t {
				return true
			}
		}

	}
	return false
}

// Blob store data about the blob file
type Blob struct {
	digest   string
	taggedIn []string // Not to be trusted that its filled in
}

func (b *Blob) Digest() string {
	return b.digest
}

func (b *Blob) Tags() []string {
	return b.taggedIn
}

func getBlobs(repo string) ([]Blob, error) {
	files, err := ioutil.ReadDir(path.Join(repo, "blobs"))
	if err != nil {
		return []Blob{}, fmt.Errorf("unable to open folder")
	}

	blobs := []Blob{}
	for _, file := range files {
		blob := Blob{}
		blob.digest = file.Name()
		blobs = append(blobs, blob)
	}

	return blobs, nil
}

// fillTagged fills a list of blobs with tags from the same image
func fillTagged(repo string, blobs []Blob) []Blob {
	dblob := map[string]Blob{}

	for _, blob := range blobs {
		dblob[blob.Digest()] = blob
	}

	tagFolder := path.Join(repo, "tags")
	files, err := ioutil.ReadDir(tagFolder)
	if err != nil {
		log.Panic(err)
	}
	for _, file := range files {
		if !validateTag(file.Name()) {
			continue
		}
		data, err := ioutil.ReadFile(path.Join(tagFolder, file.Name()))
		if err != nil {
			log.Panic(err)
		}

		schema := Schema{}
		json.Unmarshal(data, &schema)
		{
			blob, ok := dblob[schema.Config.Digest]
			if ok {
				blob.taggedIn = append(dblob[schema.Config.Digest].taggedIn, file.Name())
				dblob[schema.Config.Digest] = blob
			} else {
				log.Printf("Tag %s is most likely broken", file.Name())
			}
		}

		for _, layer := range schema.Layers {
			fmt.Println(layer.Digest)
			blob := dblob[layer.Digest]
			blob.taggedIn = append(dblob[layer.Digest].taggedIn, file.Name())
			dblob[layer.Digest] = blob
		}
	}

	blobs = []Blob{}
	for _, v := range dblob {
		blobs = append(blobs, v)
	}
	sort.Slice(blobs, func(i, j int) bool { return blobs[i].Digest() < blobs[j].Digest() })
	return blobs
}

type Tag struct {
	Schema Schema
	Name   string
}

func getTags(repo string) ([]Tag, error) {
	tagFolder := path.Join(repo, "tags")
	files, err := ioutil.ReadDir(tagFolder)
	if err != nil {
		return []Tag{}, err
	}

	tags := []Tag{}
	for _, file := range files {
		if !validateTag(file.Name()) {
			continue
		}
		data, err := ioutil.ReadFile(path.Join(tagFolder, file.Name()))
		if err != nil {
			log.Print(err)
			continue
		}

		schema := Schema{}
		json.Unmarshal(data, &schema)

		t := Tag{}
		t.Name = file.Name()
		t.Schema = schema
		tags = append(tags, t)
	}

	return tags, nil
}

func main() {
	p := pat.New()
	p.Post("/v2/{repo:.*}/manifests/{tag}", func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusOK)
	})
	p.Get("/v2/{repo:.*}/manifests/{tag}", func(w http.ResponseWriter, r *http.Request) {
		repo := r.URL.Query().Get(":repo")
		tag := r.URL.Query().Get(":tag")

		if !(validateName(repo) && (validateTag(tag) || validateDigest(tag))) {
			// Really dont have time for things
			w.WriteHeader(http.StatusNotFound)
			return
		}

		if _, err := os.Stat(repo + "/tags/" + tag); os.IsNotExist(err) {
			w.WriteHeader(http.StatusNotFound)
			return
		}

		if Accepts(r, "application/vnd.oci.image.manifest.v1+json") {
			w.Header().Set("Content-Type", "application/vnd.oci.image.manifest.v1+json")
		} else if Accepts(r, "application/vnd.docker.distribution.manifest.v2+json") {
			w.Header().Set("Content-Type", "application/vnd.docker.distribution.manifest.v2+json")
		} else {
			for _, ac := range r.Header.Values("Accept") {
				log.Printf("Client accepts %s", ac)
			}
			log.Panicf("Unable to find acceptable response type")
		}

		fileBytes, err := ioutil.ReadFile(repo + "/tags/" + tag)
		if err != nil {
			panic(err)
		}
		w.WriteHeader(http.StatusOK)
		w.Write(fileBytes)
	})
	p.Get("/v2/{repo:.*}/blobs/{digest}", func(w http.ResponseWriter, r *http.Request) {
		repo := r.URL.Query().Get(":repo")
		digest := r.URL.Query().Get(":digest")

		for k, v := range r.Header {
			fmt.Printf("%s: %s\n", k, v)
		}

		if !(validateName(repo) && validateDigest(digest)) {
			// Really dont have time for things
			return
		}

		http.ServeFile(w, r, repo+"/blobs/"+digest)
	})
	p.Put("/v2/{repo:.*}/blobs/uploads/{tag}", func(w http.ResponseWriter, r *http.Request) {
		repo := r.URL.Query().Get(":repo")
		tag := r.URL.Query().Get(":tag")
		digest := r.URL.Query().Get("digest")

		f, err := os.Open(repo + "/uploads/" + tag)
		if err != nil {
			log.Panic(err)
		}

		hash := sha256.New()
		if _, err := io.Copy(hash, f); err != nil {
			log.Panic(err)
		}
		sum := hash.Sum(nil)

		if digest == fmt.Sprintf("sha256:%x", sum) {
			log.Printf("We got a good blob \\o/")
			os.Rename(repo+"/uploads/"+tag, repo+"/blobs/"+digest)
		}

		w.WriteHeader(http.StatusCreated)

	})
	p.Put("/v2/{repo:.*}/manifests/{tag}", func(w http.ResponseWriter, r *http.Request) {
		repo := r.URL.Query().Get(":repo")
		tag := r.URL.Query().Get(":tag")

		if !(validateName(repo) && validateTag(tag)) {
			log.Print("Repo / Tag invalid")
			w.WriteHeader(http.StatusInternalServerError)
			return
		}

		err := PrepRepo(repo)
		if err != nil {
			log.Panic(err)
		}

		w.WriteHeader(http.StatusOK)

		data, err := ioutil.ReadAll(r.Body)
		if err != nil {
			log.Panic(err)
		}

		schema := Schema{}
		fmt.Printf("RAWDATA: %s\n", string(data))
		json.Unmarshal(data, &schema)
		fmt.Printf("%+v\n", schema)

		for i, l := range schema.Layers {
			if l.Size == -1 {
				fi, err := os.Stat(repo + "/blobs/" + l.Digest)
				if err != nil {
					log.Print(err)
				}
				l.Size = fi.Size()
				schema.Layers[i] = l
			}
		}
		fmt.Printf("%+v\n", schema)

		f, err := os.Create(repo + "/tags/" + tag)
		if err != nil {
			log.Panic(err)
		}
		defer f.Close()

		newData, err := json.Marshal(schema)
		if err != nil {
			log.Panic(err)
		}
		_, err = f.Write(newData)
		if err != nil {
			log.Panic(err)
		}

		hash := sha256.New()
		hash.Write(newData)
		sum := hash.Sum(nil)

		if _, err := os.Stat(repo + "/tags/" + fmt.Sprintf("sha256:%x", sum)); os.IsNotExist(err) {
			err = os.Symlink(tag, repo+"/tags/"+fmt.Sprintf("sha256:%x", sum))
			if err != nil {
				log.Panic(err)
			}
		}

	})
	p.Head("/v2/{repo:.*}/manifests/{tag}", func(w http.ResponseWriter, r *http.Request) {
		repo := r.URL.Query().Get(":repo")
		tag := r.URL.Query().Get(":tag")
		if !(validateName(repo) && (validateTag(tag) || validateDigest(tag))) {
			w.WriteHeader(http.StatusNotFound)
			return
		}

		if _, err := os.Stat(repo + "/tags/" + tag); os.IsNotExist(err) {
			w.WriteHeader(http.StatusNotFound)
			return
		}
		w.WriteHeader(http.StatusOK)
	})
	p.Head("/v2/{repo:.*}/blobs/{digest}", func(w http.ResponseWriter, r *http.Request) {
		repo := r.URL.Query().Get(":repo")
		digest := r.URL.Query().Get(":digest")

		if !(validateName(repo) && validateDigest(digest)) {
			return
		}

		if _, err := os.Stat(repo + "/blobs/" + digest); os.IsNotExist(err) {
			w.WriteHeader(http.StatusNotFound)
			return
			// path/to/whatever does not exist
		}

		w.WriteHeader(http.StatusOK)
	})
	p.Get("/v2/", func(w http.ResponseWriter, r *http.Request) {
	})
	p.Get("/", func(w http.ResponseWriter, r *http.Request) {
		repo := strings.Trim(r.URL.Path, "/")
		tags, err := getTags(repo)
		if err == nil {
			if !validateName(repo) {
				return
			}
			if err != nil {
				return
			}
			blobs, err := getBlobs(repo)
			if err == nil && len(blobs) > 0 {
				blobs = fillTagged(repo, blobs)
				filedata := struct {
					Name  string
					Blobs []Blob
					Tags  []Tag
				}{Blobs: blobs, Tags: tags, Name: repo}
				tmpl, err := template.New("foo").ParseFiles("/opt/template/imageview.html")
				if err != nil {
					log.Fatal(err)
				}
				err = tmpl.ExecuteTemplate(w, "imageview.html", filedata)
				if err != nil {
					log.Fatal(err)
				}
				return
			}
		}

		files, err := ioutil.ReadDir("." + r.URL.Path)
		if err != nil {
			log.Panic(err)
		}

		tmpl, err := template.New("foo").ParseFiles("/opt/template/libraryview.html")
		if err != nil {
			log.Fatal(err)
		}
		filesdata := struct {
			Name      string
			Libraries []fs.FileInfo
			Images    []fs.FileInfo
		}{}
		filesdata.Name = repo
		for _, file := range files {
			_, err := getTags(path.Join(repo, file.Name()))
			if err == nil {
				filesdata.Images = append(filesdata.Images, file)
			} else {
				filesdata.Libraries = append(filesdata.Libraries, file)
			}
		}
		err = tmpl.ExecuteTemplate(w, "libraryview.html", filesdata)
		if err != nil {
			log.Fatal(err)
		}
	})

	p.Patch("/v2/{repo:.*}/blobs/uploads/{guid}", func(w http.ResponseWriter, r *http.Request) {
		repo := r.URL.Query().Get(":repo")
		guid := r.URL.Query().Get(":guid")

		if !(validateName(repo)) {
			return
		}

		err := PrepRepo(repo)
		if err != nil {
			log.Panic(err)
		}

		tmpfile, err := os.Create(repo + "/uploads/" + guid)
		if err != nil {
			log.Panic(err)
		}
		defer tmpfile.Close()
		_, err = io.Copy(tmpfile, r.Body)
		if err != nil {
			log.Panic(err)
		}

		w.Header().Add("Location", "/v2/"+repo+"/blobs/uploads/"+guid)
		w.WriteHeader(http.StatusAccepted)
	})
	p.Post("/v2/{repo:.*}/blobs/uploads/", func(w http.ResponseWriter, r *http.Request) {
		repo := r.URL.Query().Get(":repo")

		if !(validateName(repo)) {
			return
		}

		w.Header().Add("Location", "/v2/"+repo+"/blobs/uploads/"+uuid.New().String())
		w.WriteHeader(http.StatusAccepted)
	})

	log.Fatal(http.ListenAndServe(":5000", handlers.LoggingHandler(os.Stdout, p)))
}
