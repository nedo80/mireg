APPLICATION = mireg

REGISTRY ?= mireg.wr25.org
REPOGROUP ?=
KUBECTLOPTS ?=
RELEASE ?= latest
DOCKERCOMMAND ?= podman

GOCODE := $(shell find . -name \*.go -print)
TEMPLATECODE := $(shell find template -print)

BUILD_NUMBER := $(shell date +%Y%m%d%H%M%S)

.PHONY: clean

all: helm

$(APPLICATION): $(GOCODE) Makefile
	go test
	go build .
	mc cp mireg reflink/artifacts/mireg/mireg-$(BUILD_NUMBER)
	sed -i "s/miregversion:.*/miregversion: '${BUILD_NUMBER}'/g" helm/values.yaml

$(APPLICATION)-gui.tgz: $(TEMPLATECODE)
	tar c template | gzip > $(APPLICATION)-gui.tgz
	mc cp $(APPLICATION)-gui.tgz reflink/artifacts/mireg/mireg-gui-$(BUILD_NUMBER).tgz
	sed -i "s/miregguiversion:.*/miregguiversion: '${BUILD_NUMBER}'/g" helm/values.yaml


docker.digest: Dockerfile
	$(DOCKERCOMMAND) build -t $(REGISTRY)$(REPOGROUP)/$(APPLICATION):$(BUILD_NUMBER) .
	$(DOCKERCOMMAND) push $(REGISTRY)$(REPOGROUP)/$(APPLICATION):$(BUILD_NUMBER)
	sed -i "s/miregcontainertag:.*/miregcontainertag: '${BUILD_NUMBER}'/g" helm/values.yaml
	touch docker.digest

helm: docker.digest $(APPLICATION)-gui.tgz $(APPLICATION)
	helm upgrade mireg helm


clean:
	rm $(APPLICATION) ${APPLICATION}-gui.tgz docker.digest -f
